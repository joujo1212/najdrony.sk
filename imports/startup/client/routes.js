import React from 'react';
import { Router, Route, browserHistory } from 'react-router';

import { ROUTES } from '/imports/utils/constants';
// route components
import App from '../../ui/App';
import HomePage from '/imports/ui/pages/HomePage/HomePage';
import PolicyPage from '/imports/ui/pages/PolicyPage/PolicyPage';
import AboutUsPage from '/imports/ui/pages/AboutUsPage/AboutUsPage';
import CountdownPage from '/imports/ui/pages/CountdownPage/CountdownPage';
import MaintenancePage from '/imports/ui/pages/MaintenancePage/MaintenancePage';
import NotFoundPage from '/imports/ui/pages/NotFoundPage/NotFoundPage';

// import NotFoundPage from '../../ui/pages/NotFoundPage.js';

const MAINTENANCE = true;
const COUNTDOWN = true;

const commonRoutes =
<Route path={ROUTES.HOME_PAGE} component={App}>
    <Route path={ROUTES.PRODUCTS} component={HomePage}/>
    <Route path={ROUTES.POLICY} component={PolicyPage}/>
    <Route path={ROUTES.ABOUT_US} component={AboutUsPage}/>
    <Route path="*" component={NotFoundPage}/>
</Route>;

const specialRoutes = COUNTDOWN ? <Route path="*" component={CountdownPage}/>
    : <Route path="*" component={MaintenancePage}/>;

export const renderRoutes = () => (
    <Router history={browserHistory}>
        {!MAINTENANCE ? commonRoutes : specialRoutes}
    </Router>
);