import React, { Component } from 'react';
import Header from '/imports/ui/components/Header/Header';
import css from './style.scss';

// App component - represents the whole app
export default class App extends Component {
    render() {
        return (
            <div>
                <Header/>
                <div className={css.container}>
                    {this.props.children}
                </div>
            </div>
        );
    }
}