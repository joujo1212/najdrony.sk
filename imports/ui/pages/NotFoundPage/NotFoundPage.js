import React, { Component } from 'react';
import css from './style.scss';

export default class NotFoundPage extends Component {
    render() {
        return (
            <div>
                Chyba 404 - stránka nebola nájdená
            </div>
        );
    }
}