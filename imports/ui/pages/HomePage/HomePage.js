import React, { Component } from 'react';
import css from './style.scss';

import Product from '/imports/ui/components/Product/Product';
import CategoriesMenu from '/imports/ui/components/CategoriesMenu/CategoriesMenu';

// App component - represents the whole app
export default class HomePage extends Component {

    render() {
        const mockData = [
            {
                imgSrc: 'http://www.modelarina.cz/images/rc-letadla/pelikan-Skylady-III_01.jpg',
                name: 'Sky Lady III, plyn vpravo',
                price: 18.9
            },
            {
                imgSrc: 'http://www.modelarina.cz/images/rc-letadla/pelikan-Skylady-III_01.jpg',
                name: 'Sky Lady III, plyn vpravo',
                price: 18.9
            },
            {
                imgSrc: 'http://www.modelarina.cz/images/rc-letadla/pelikan-Skylady-III_01.jpg',
                name: 'Sky Lady III, plyn vpravo',
                price: 18.9
            },
            {
                imgSrc: 'http://www.modelarina.cz/images/rc-letadla/pelikan-Skylady-III_01.jpg',
                name: 'Sky Lady III, plyn vpravo',
                price: 18.9
            },
        ];
        return (
            <div className={css.container}>
                <div className={css.menu}>
                    <CategoriesMenu/>
                </div>
                <div className={css.products}>
                    {mockData.map( (item, i) =>
                        <Product key={i} imgSrc={item.imgSrc} name={item.name} price={item.price}/>
                    )}
                </div>
            </div>
        );
    }
}