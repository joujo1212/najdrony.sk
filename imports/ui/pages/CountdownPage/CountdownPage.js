import React, { Component } from 'react';
import css from './style.scss';
import Logo from '/imports/ui/components/Logo/Logo';
import ShareButtons from '/imports/ui/components/ShareButtons/ShareButtons';

export default class CountdownPage extends Component {

    constructor() {
        super();
        this.state = {
            days: 0,
            hours: 0,
            minutes: 0,
            seconds: 0
        };
        this.timer = null;
    }

    showRemaining() {
        var _second = 1000;
        var _minute = _second * 60;
        var  _hour = _minute * 60;
        var  _day = _hour * 24;

        var now = new Date();
        var end = new Date('10/20/2016 10:1 AM');
        var distance = end - now;
        if (distance < 0) {
            clearInterval(this.timer);
            return;
        }
        var days = Math.floor(distance / _day);
        var hours = Math.floor((distance % _day) / _hour);
        var minutes = Math.floor((distance % _hour) / _minute);
        var seconds = Math.floor((distance % _minute) / _second);

        this.setState({
            days: days,
            hours: hours,
            minutes: minutes,
            seconds: seconds
        });
    }

    componentDidMount() {
        $.backstretch('/countdown_bg.jpg');
        this.showRemaining();
        this.timer = setInterval(this.showRemaining.bind(this), 1000);
    }

    render() {

        return (
            <div className={css.container}>
                <div className={css.logoWrapper}>
                    <Logo />
                </div>
                <div className={css.title}>Stránku spúšťame o</div>
                <div id="countdown" className={css.countdown}>
                    <div className={css.part}><div className={css.value}>{this.state.days}</div><div className={css.label}>dní</div></div>
                    <div className={css.part}><div className={css.value}>{this.state.hours}</div><div className={css.label}>hodín</div></div>
                    <div className={css.part}><div className={css.value}>{this.state.minutes}</div><div className={css.label}>minút</div></div>
                    <div className={css.part}><div className={css.value}>{this.state.seconds}</div><div className={css.label}>sekúnd</div></div>
                </div>
                <ShareButtons/>
            </div>
        );
    }
}