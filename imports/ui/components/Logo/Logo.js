import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router'

import css from './style.scss';
import { ROUTES } from '/imports/utils/constants';

export default class Logo extends Component {
    render() {
        return (
            <h1 className={css.wrapper}>
                <Link to={ROUTES.HOME_PAGE}>
                    <img src="logo.svg" width="94px" height="94px" />
                    <div className={css.title}>Najdrony.sk</div>
                </Link>
            </h1>
        );
    }
}