import React, { Component, PropTypes } from 'react';
import css from './style.scss';

export default class CategoriesMenu extends Component {
    render() {
        const mockData = [
            'RTF Sety',
            'Hotové modely',
            'Modely do parku',
            'Vetrone',
            'Bezmotorové modely',
            'RC Sety',
            'Príslušenstvo',
            'Ostatné modely',
            'Výpredaj!'
        ];
        return (
            <div className={css.container}>
                <div className={css.content}>
                    Categories menu
                    <ul>
                        {mockData.map( (item, i) => <li key={i}>{item}</li>)}
                    </ul>
                </div>
            </div>
        );
    }
}