import React, { Component, PropTypes } from 'react';
import css from './style.scss';
import HeaderLink from '/imports/ui/components/HeaderLink/HeaderLink';
import Logo from '/imports/ui/components/Logo/Logo';
import { ROUTES } from '/imports/utils/constants';

export default class Header extends Component {
    render() {
        return (
            <div className={css.wrapper}>
                <div className={css.content}>
                    <Logo />
                    <div className={css.topMenu}>
                        <HeaderLink label="Doprava" link={ROUTES.POLICY} />
                        <HeaderLink label="O nas" link={ROUTES.ABOUT_US} />
                        <HeaderLink label="Moj ucet" link={ROUTES.MY_ACCOUNT} />
                    </div>
                </div>
            </div>
        );
    }
}