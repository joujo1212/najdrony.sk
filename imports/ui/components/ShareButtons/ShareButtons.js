import React, {Component, PropTypes} from 'react';
import css from './style.scss';

export default class ShareButtons extends Component {
    render() {
        return (
            <div className={css.wrapper}>
                <div className="fb-share-button" data-href="http://eshop.najdrony.sk:3000/" data-layout="button" data-size="large" data-mobile-iframe="true"><a className="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Feshop.najdrony.sk%3A3000%2F&amp;src=sdkpreparse">Zdieľať</a></div>
                <div className="g-plus" data-action="share" data-annotation="none" data-height="24" data-href="http://eshop.najdrony.sk:3000"></div>
            </div>
        );
    }
};
