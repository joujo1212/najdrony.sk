import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';

import css from './style.scss';

export default class HeaderLink extends Component {
    render() {
        return (
            <div className={css.link}>
                <div className={css.content}>
                    <Link to={this.props.link}>{this.props.label}</Link>
                </div>
            </div>
        );
    }
}

HeaderLink.propTypes = {
    label: PropTypes.string.isRequired,
    link: PropTypes.string.isRequired
};