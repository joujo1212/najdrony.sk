import React, {Component, PropTypes} from 'react';
import css from './style.scss';

export default class Product extends Component {
    constructor(props) {
        super(props);
        this.state = {showBuyButton: false};
    }

    onMouserEnter() {
        this.setState({showBuyButton: true});
    }

    onMouseLeave() {
        this.setState({showBuyButton: false});
    }

    addToBasket() {
        alert('Pridal si si produkt do kosika');
    }

    render() {
        const buyButton = this.state.showBuyButton === true ?
            <div className={css.buyButton} onClick={this.addToBasket}>Pridať do košíka</div> :
            <div className={css.price}>{this.props.price} &euro;</div>;
        return (
            <div className={css.wrapper}>
                <div className={css.product} onMouseEnter={this.onMouserEnter.bind(this)}
                     onMouseLeave={this.onMouseLeave.bind(this)}>
                    <img src={this.props.imgSrc}/>
                    <div className={css.title}>{this.props.name}</div>
                    {buyButton}
                </div>
            </div>
        );
    }
};

Product.propTypes = {
    imgSrc: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired
};
