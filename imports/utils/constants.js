export const ROUTES = {
    HOME_PAGE: '',
    PRODUCTS: '/',
    ABOUT_US: '/o-nas',
    POLICY: '/obchodne-podmienky',
    MY_ACCOUNT: '/moj-ucet',
};