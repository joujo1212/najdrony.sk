#Meteor Plugins
##Routing
run npm install --save react-router
See https://guide.meteor.com/react.html#using-react-router
See https://github.com/reactjs/react-router/blob/master/examples/route-no-match/app.js

##SASS + CSS Modules
removed standard-minifiers
run meteor npm install --save-dev node-sass
added nathantreid:css-modules
See https://github.com/nathantreid/meteor-css-modules/wiki/Sass-and-React-Toolbox-Support

##React component mixed with Blaze
See https://guide.meteor.com/react.html#react-in-blaze


##Countdown page
added goltfisch:jquery-backstretch
See https://atmospherejs.com/goltfisch/jquery-backstretch

#TODO
- Logo in Safari is not vector (maybe some header meta tag should be there for safari?)
- Meteor SEO e.g. http://www.manuel-schoebel.com/blog/meteor-and-seo
- Complete deploy proccess (--production switch is not good!), see https://johngibby.com/blog/How_to_deploy_your_meteor.js_app_on_Digital_Ocean
